// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GameManager } from "./GameManager";

const {ccclass, property} = cc._decorator;

@ccclass
export class NotifyPopup extends cc.Component 
{
    @property(cc.Label)
    title: cc.Label = null;

    @property(cc.Button)
    okButton: cc.Button = null;

    public showNotify(title: string, okHandler: cc.Component.EventHandler)
    {
        if(title == '')return;

        this.title.string = title;

        this.okButton.clickEvents.splice(0, this.okButton.clickEvents.length);

        this.okButton.clickEvents.push(okHandler);

        GameManager.instance.addClickEventHandler(this.okButton, this.node, 'NotifyPopup', 'hidePopup');

        this.node.active = true;
    }

    hidePopup()
    {
        this.node.active = false;
    }
}
