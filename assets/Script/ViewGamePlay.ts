// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GameManager, GameState } from "./GameManager";
import { Puzzle } from "./Puzzle";
import { UserData } from "./UserData";
import { ViewExam } from "./ViewExam";
import { ViewSubject } from "./ViewSubject";

const {ccclass, property} = cc._decorator;

export class AnswerData
{
    answerString: string = 'Hello';

    correct: boolean = false;
}

export class PuzzleData
{
    questionString: string = 'Hello';

    listAnswer: AnswerData[] = [];
}

export class SubjectData
{
    subjectId: number = 0;

    subjectName: string = 'Math';

    listPuzzle: PuzzleData[] = [];
}

enum ViewGamePlayState
{
    None,
    ChoiceSubject,
    Exam,
    Result
}

@ccclass
export class ViewGamePlay extends cc.Component
{
    public static instance: ViewGamePlay;

    @property(ViewSubject)
    viewSubject: ViewSubject = null;

    @property(ViewExam)
    viewExam: ViewExam = null;

    @property(cc.Label)
    userName: cc.Label = null;

    @property(cc.Button)
    exitButton: cc.Button = null;

    @property
    exitTitle: string = 'Do you want to exit?';

    listSubject: SubjectData[] = [];

    puzzlePool: Puzzle[] = [];

    currentSubject: SubjectData;

    resultExam: string = '0/0';

    _state: ViewGamePlayState = ViewGamePlayState.None;

    onEnable()
    {
        this.onChangeState(ViewGamePlayState.ChoiceSubject);
        this.userName.string = UserData.instance.getCurrentUserName();
    }

    start()
    {
        GameManager.instance.addClickEventHandler(this.exitButton, this.node, 'ViewGamePlay', 'onClickExitButton');
    }

    onChangeState(state: ViewGamePlayState)
    {
        if(this._state == state) return;
        this._state = state;

        switch(state)
        {
            case ViewGamePlayState.ChoiceSubject:
                this.showChoiceSubject();
                break;

            case ViewGamePlayState.Exam:
                this.showExam();
                break;

            case ViewGamePlayState.Result:
                this.showResult();
                break;
        }
    }

    showChoiceSubject()
    {
        this.viewExam.viewHide();
        this.viewSubject.loadData(this.listSubject);
    }

    showExam()
    {
        this.viewSubject.viewHide();

        this.viewExam.loadData(this.currentSubject.listPuzzle);
    }

    showResult()
    { 
        var title = 'you got '.concat(this.resultExam).concat(' questions correct');

        GameManager.instance.showNotiPopup(title, this.node, 'ViewGamePlay', 'changeStateChoiceSubject');
    }

    createChangeStateExam(): cc.Component.EventHandler
    {
        return GameManager.instance.createClickEventHandler(this.node, 'ViewGamePlay', 'changeStateExam');
    }

    changeStateExam()
    {
        this.onChangeState(ViewGamePlayState.Exam);
    }

    changeStateChoiceSubject()
    {
        this.onChangeState(ViewGamePlayState.ChoiceSubject);
    }

    onClickExitButton()
    {
        GameManager.instance.showYesNoPopup(this.exitTitle, this.node, 'ViewGamePlay', 'changeViewLogin');
    }

    changeViewLogin()
    {
        GameManager.instance.onChangeState(GameState.Login);
    }
}
