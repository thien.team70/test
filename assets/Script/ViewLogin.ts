// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GameManager, GameState } from "./GameManager";
import { UserData } from "./UserData";

const {ccclass, property} = cc._decorator;

enum ViewLoginState
{
    None,
    Login,
    Register,
    Forget
}

@ccclass
export class ViewLogin extends cc.Component {

    @property(cc.Label)
    title: cc.Label = null;

    @property(cc.Label)
    usernameTitle: cc.Label = null;

    @property(cc.EditBox)
    usernameBox: cc.EditBox = null;

    @property(cc.Label)
    passwordTitle: cc.Label = null;

    @property(cc.EditBox)
    passwordBox: cc.EditBox = null;

    @property(cc.Node)
    viewLogin: cc.Node = null;

    @property(cc.Node)
    viewRegister: cc.Node = null;

    @property(cc.Node)
    viewForget: cc.Node = null;
    
    _state: ViewLoginState = ViewLoginState.None;

    onEnable() 
    {
        this.onChangeState(ViewLoginState.Login);
    }

    onDisable()
    {
        this._state = ViewLoginState.None;
    }

    onChangeState(state: ViewLoginState)
    {
        if(this._state == state) return;
        this._state = state;

        this.usernameBox.string = '';
        this.passwordBox.string = '';

        switch(state)
        {
            case ViewLoginState.Login:
                this.ChangeViewLogin();
                break;

            case ViewLoginState.Register:
                this.ChangeViewRegister();
                break;

            case ViewLoginState.Forget:
                this.ChangeViewForget();
                break;
        }
    }

    ChangeViewLogin()
    {
        this.title.string = "Login";

        this.viewLogin.active = true;

        this.viewRegister.active = false;
        this.viewForget.active = false;

        this.passwordTitle.node.active = true;
        this.passwordBox.node.active = true;
    }

    ChangeViewRegister()
    {
        this.title.string = "Register";

        this.viewRegister.active = true;

        this.viewLogin.active = false;
        this.viewForget.active = false;

        this.passwordTitle.node.active = true;
        this.passwordBox.node.active = true;
    }

    ChangeViewForget()
    {
        this.title.string = "Forget";

        this.viewForget.active = true;

        this.viewLogin.active = false;
        this.viewRegister.active = false;

        this.passwordTitle.node.active = false;
        this.passwordBox.node.active = false;
    }
    
    onClickSaveButton()
    {
        var title = UserData.instance.createAccount(this.usernameBox.string, this.passwordBox.string);
        GameManager.instance.showNotiPopup(title, this.node, "ViewLogin", "onClickBackButton");
    }

    onClickForgetButton()
    {
        this.onChangeState(ViewLoginState.Forget);
    }

    onClickRegisterButton()
    {
        this.onChangeState(ViewLoginState.Register);
    }

    onClickLoginButton()
    {
        var userName = this.usernameBox.string;
        var pass = this.passwordBox.string;
        if(UserData.instance.checkAccount(userName, pass))
        {
            UserData.instance.setCurrentData(userName);
            GameManager.instance.onChangeState(GameState.GamePlay);
            return;
        }

        var title = "User name and password are wrong";
        GameManager.instance.showNotiPopup(title, this.node, "ViewLogin", "onClickBackButton");
    }

    onClickFindButton()
    {
        var title = UserData.instance.getPassword(this.usernameBox.string);
        GameManager.instance.showNotiPopup(title, this.node, "ViewLogin", "onClickBackButton");
    }

    onClickBackButton()
    {
        this.onChangeState(ViewLoginState.Login);
    }
}
