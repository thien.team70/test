// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { Answer } from "./Answer";
import { GameManager } from "./GameManager";
import { PuzzleIndex } from "./PuzzleIndex";
import { AnswerData, PuzzleData } from "./ViewGamePlay";

const {ccclass, property} = cc._decorator;

@ccclass
export class Puzzle extends cc.Component 
{
    @property(cc.Prefab)
    answerPrefab: cc.Prefab = null;

    @property(cc.Node)
    prefabContent: cc.Node = null;

    @property(cc.Label)
    questionName: cc.Label = null;

    answerPool: Array<Answer> = new Array();

    dataLength: number = 0;

    puzzleIndex: PuzzleIndex = null;

    loadData(puzzleData: PuzzleData, puzzleIdx: PuzzleIndex)
    {
        this.node.active = true;

        this.puzzleIndex = puzzleIdx;

        this.questionName.string = puzzleData.questionString;

        var listAnswerData = puzzleData.listAnswer;
        var poolLength = this.answerPool.length;
        this.dataLength = listAnswerData.length;

        var resetListAnswer = GameManager.instance.createClickEventHandler(this.node, 'Puzzle', 'resetAnswers');
        var checkCorrect = GameManager.instance.createClickEventHandler(this.node, 'Puzzle', 'checkCorrect');

        for (var i = 0; i < this.dataLength; i++) 
        {
            var data = listAnswerData[i];

            if(i < poolLength)
            {
                this.answerPool[i].loadData(data, resetListAnswer, checkCorrect);
                continue;
            }

            var prefab = cc.instantiate(this.answerPrefab);
            prefab.parent = this.prefabContent;

            var answer = prefab.getComponent(Answer);
            this.answerPool.push(answer);

            answer.loadData(data, resetListAnswer, checkCorrect);
        }

        for(var i = this.dataLength; i < poolLength; i++)
        {
            this.answerPool[i].node.active = false;
        }
    }

    checkCorrect()
    {
        if(this.puzzleIndex == null) return;

        for(var i = 0; i < this.dataLength; i++)
        {
            var answer = this.answerPool[i];

            if(answer.isToggle == false) continue; 
            
            this.puzzleIndex.setBG(true);
            this.puzzleIndex.correct = answer.correct;
            this.puzzleIndex.answerChoiceIndex = i;
            return;
        }

        this.puzzleIndex.setBG(false);
        this.puzzleIndex.correct = false;
    }

    resetAnswers()
    {
        for(var i = 0; i < this.dataLength; i++)
        {
            this.answerPool[i].onReset();
        }
    }
}
