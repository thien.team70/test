// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GameManager, GameState } from "./GameManager";
import { ViewGamePlay } from "./ViewGamePlay";

const {ccclass, property} = cc._decorator;

@ccclass
export class ViewLoadGame extends cc.Component 
{

    @property(cc.ProgressBar)
    progressBar: cc.ProgressBar = null;

    @property
    dataUrl: string = 'https://drive.google.com/u/0/uc?id=1SsR-4cwQ_lrDdimr5v777VGYV3PVs5YO&export=download';

    @property
    dataEmbed: string = 'listSubjectData';

    start () 
    {
        this.progressBar.progress = 1;
        //cc.assetManager.downloader.download('Download_Data', this.dataUrl, 'string', {onFileProgress: this.setProgress }, this.onCompleteDownload);
        cc.resources.load<cc.TextAsset>(this.dataEmbed, this.onCompleteLoadEmbed);
    }

    onCompleteDownload(error: Error, content: string)
    {
        if(error != null)
        {
            cc.resources.load<cc.TextAsset>(this.dataEmbed, this.onCompleteLoadEmbed);
            return;
        }

        ViewGamePlay.instance.listSubject = JSON.parse(content);

        GameManager.instance.onChangeState(GameState.Login);
    }

    onCompleteLoadEmbed(error: Error, assets: cc.TextAsset)
    {
        if(error != null)
        {
            return;
        }
        
        ViewGamePlay.instance.listSubject = JSON.parse(assets.text);

        GameManager.instance.onChangeState(GameState.Login);
    }

    setProgress(loaded: any, total: any)
    {
        loaded = Number.parseInt(loaded);
        total = Number.parseInt(total);
    }
}
