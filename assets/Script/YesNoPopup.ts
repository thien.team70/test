// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GameManager } from "./GameManager";

const {ccclass, property} = cc._decorator;

@ccclass
export class YesNoPopup extends cc.Component {

    @property(cc.Label)
    title: cc.Label = null;

    @property(cc.Button)
    yesButton: cc.Button = null;

    @property(cc.Button)
    noButton: cc.Button = null;

    start () 
    {
        GameManager.instance.addClickEventHandler(this.noButton, this.node, 'YesNoPopup', 'hidePopup');
    }

    public showNotify(title: string, yesHandler: cc.Component.EventHandler)
    {
        if(title == '')return;

        this.title.string = title;

        this.yesButton.clickEvents.splice(0, this.yesButton.clickEvents.length);

        this.yesButton.clickEvents.push(yesHandler);

        GameManager.instance.addClickEventHandler(this.yesButton, this.node, 'YesNoPopup', 'hidePopup');

        this.node.active = true;
    }

    hidePopup()
    {
        this.node.active = false;
    }
}
