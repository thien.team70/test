// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
import { NotifyPopup } from './NotifyPopup';
import { ViewGamePlay } from './ViewGamePlay';
import { ViewLoadGame } from './ViewLoadGame';
import { ViewLogin } from './ViewLogin';
import { YesNoPopup } from './YesNoPopup';

export enum GameState
{
    None,
    Load,
    Login,
    GamePlay
}

@ccclass
export class GameManager extends cc.Component 
{
    public static instance: GameManager = null;

    @property(NotifyPopup)
    notifyPopup: NotifyPopup;

    @property(YesNoPopup)
    yesNoPopup: YesNoPopup;

    @property(ViewLoadGame)
    viewLoadGame: ViewLoadGame;

    @property(ViewLogin)
    viewLogin: ViewLogin;

    @property(ViewGamePlay)
    viewGamePlay: ViewGamePlay;

    _state: GameState = GameState.None;

    onLoad () 
    {
        if(GameManager.instance != null) GameManager.instance.destroy();
        GameManager.instance = this;

        if(ViewGamePlay.instance != null) ViewGamePlay.instance.destroy();
        ViewGamePlay.instance = this.viewGamePlay;

    }

    start () 
    {
        this.onChangeState(GameState.Load);
    }

    onChangeState(state: GameState)
    {
        if(this._state == state) return;
        this._state = state;

        switch(state)
        {
            case GameState.Load:
                this.showViewLoadGame();
                break;

            case GameState.Login:
                this.showViewLogin();
                break;

            case GameState.GamePlay:
                this.showViewGamePlay();
                break;
        }
    }

    showViewLoadGame()
    {
        this.viewLoadGame.node.active = true;

        this.viewLogin.node.active = false;
        this.viewGamePlay.node.active = false;
    }

    showViewLogin()
    {
        this.viewLogin.node.active = true;

        this.viewLoadGame.node.active = false;
        this.viewGamePlay.node.active = false;
    }

    showViewGamePlay()
    {
        this.viewGamePlay.node.active = true;

        this.viewLoadGame.node.active = false;
        this.viewLogin.node.active = false;
    }

    addClickEventHandler(button: cc.Button, target: cc.Node, component: string, callBack: string)
    {
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = target;
        clickEventHandler.component = component;
        clickEventHandler.handler = callBack;

        button.clickEvents.push(clickEventHandler);
    }

    createClickEventHandler(target: cc.Node, component: string, callBack: string): cc.Component.EventHandler
    {
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = target;
        clickEventHandler.component = component;
        clickEventHandler.handler = callBack;

        return clickEventHandler;
    }

    showNotiPopup(title: string, target: cc.Node, component: string, callBack: string)
    {
        var okHandler = this.createClickEventHandler(target, component, callBack);

        this.notifyPopup.showNotify(title, okHandler);
    }

    showYesNoPopup(title: string, target: cc.Node, component: string, callBack: string)
    {
        var yesHandler = this.createClickEventHandler(target, component, callBack);

        this.yesNoPopup.showNotify(title, yesHandler);
    }
}
