// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

class Data
{
    username: string;
    password: string;
}

@ccclass
export class UserData extends cc.Component 
{
    public static instance: UserData = null;

    listUserData: Data[] = [];

    currentData: Data = null;

    onLoad() 
    {
        if(UserData.instance != null) UserData.instance.destroy();

        UserData.instance = this;

        var data = localStorage.getItem('listUserData');
        if(data != null) this.listUserData = JSON.parse(data);
    }

    createAccount(name: string, pass: string): string
    {
        if(name.length == 0 || pass.length == 0) return "User name and password are invalid";

        for(var data of this.listUserData)
        {
            if(data.username == name) return "User name is exist";
        }

        var newData = new Data();
        newData.username = name;
        newData.password = pass;

        this.listUserData.push(newData);
        
        localStorage.setItem('listUserData', JSON.stringify(this.listUserData));

        return "Create account success";
    }

    checkAccount(name: string, pass: string): boolean
    {
        if(name.length == 0 || pass.length == 0) return false;

        for(var data of this.listUserData)
        {
            if(data.username == name && data.password == pass) return true;
        }

        return false;
    }

    getPassword(name: string): string
    {
        if(name.length == 0) return "User name is invalid";

        for(var data of this.listUserData)
        {
            if(data.username == name) return 'Password \n '.concat(data.password.toString());
        }

        return "User name is not exist";
    }

    setCurrentData(name: string)
    {
        for(var data of this.listUserData)
        {
            if(data.username != name) continue;
            
            this.currentData = data;
            return;
        }
    }

    getCurrentUserName(): string
    {
        if(this.currentData == null) return '';

        return this.currentData.username;
    }
}
