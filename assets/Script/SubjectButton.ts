// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GameManager } from "./GameManager";
import { SubjectData, ViewGamePlay } from "./ViewGamePlay";

const {ccclass, property} = cc._decorator;

@ccclass
export class SubjectButton extends cc.Component {

    @property(cc.Button)
    button: cc.Button = null;

    @property(cc.Label)
    subjectName: cc.Label = null;

    _subjectData: SubjectData;

    _inited: boolean = false;

    loadData(subjectData: SubjectData)
    {
        this._subjectData = subjectData;
        
        this.subjectName.string = subjectData.subjectName.toString();

        this.node.active = true;

        if(this._inited) return;
        this._inited = true;

        GameManager.instance.addClickEventHandler(this.button, this.node, 'SubjectButton', 'onClick');

        this.button.clickEvents.push(ViewGamePlay.instance.createChangeStateExam());
    }

    onClick()
    {
        ViewGamePlay.instance.currentSubject = this._subjectData;
    }
}
