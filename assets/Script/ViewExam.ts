// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GameManager } from "./GameManager";
import { Puzzle } from "./Puzzle";
import { PuzzleIndex } from "./PuzzleIndex";
import { PuzzleData, ViewGamePlay } from "./ViewGamePlay";

const {ccclass, property} = cc._decorator;

@ccclass
export class ViewExam extends cc.Component {

    @property(cc.Prefab)
    questionPrefab: cc.Prefab = null;

    @property(cc.Node)
    prefabContent: cc.Node = null;

    @property(cc.Button)
    resultButton: cc.Button = null;

    @property(Puzzle)
    puzzlePrefab: Puzzle = null;

    @property
    finishTitle: string = 'Do you want to finish the test?';

    puzzlePool: Array<PuzzleIndex> = new Array();

    listPuzzle: Array<PuzzleData> = new Array();

    start()
    {
        GameManager.instance.addClickEventHandler(this.resultButton, this.node, 'ViewExam', 'onClickResultButton');
    }

    loadData(listData: Array<PuzzleData>)
    {
        var poolLength = this.puzzlePool.length;
        this.listPuzzle = listData;
        
        var refreshListPuzzle = GameManager.instance.createClickEventHandler(this.node, 'ViewExam', 'refreshListPuzzle');
        var showPuzzle = GameManager.instance.createClickEventHandler(this.node, 'ViewExam', 'showPuzzle');

        for (var i = 0; i < listData.length; i++) 
        {
            if(i < poolLength)
            {
                this.puzzlePool[i].loadData(i, refreshListPuzzle, showPuzzle);
                continue;
            }

            var prefab = cc.instantiate(this.questionPrefab);
            prefab.parent = this.prefabContent;

            var puzzle = prefab.getComponent(PuzzleIndex);
            this.puzzlePool.push(puzzle);

            puzzle.loadData(i, refreshListPuzzle, showPuzzle);
        }

        for(var i = listData.length; i < poolLength; i++)
        {
            this.puzzlePool[i].node.active = false;
        }

        this.viewShow();
    }

    viewShow()
    {
        this.node.active = true;

        this.puzzlePool[0].onClickButton();

        this.showPuzzle();
    }

    viewHide()
    {
        this.node.active = false;
    }

    refreshListPuzzle()
    {
        for(var i = 0; i < this.listPuzzle.length; i++)
        {   
            this.puzzlePool[i].onReset();
        }
    }

    showPuzzle()
    {
        for(var i = 0; i < this.listPuzzle.length; i++)
        {   
            var puzzleIdx = this.puzzlePool[i];
            if(puzzleIdx.isToggle == false) continue;

            var data = this.listPuzzle[puzzleIdx._index];
            this.puzzlePrefab.loadData(data, puzzleIdx);

            if(puzzleIdx.answerChoiceIndex < 0) return;

            this.puzzlePrefab.answerPool[puzzleIdx.answerChoiceIndex].onClickButton();
        }
    }

    onClickResultButton()
    {
        GameManager.instance.showYesNoPopup(this.finishTitle, this.node, 'ViewExam', 'showScore');
    }

    showScore()
    {
        ViewGamePlay.instance.resultExam = this.getResultExam();

        ViewGamePlay.instance.showResult();
    }

    getResultExam(): string
    {
        var result = 0;

        for(var i = 0; i < this.listPuzzle.length; i++)
        {   
            var puzzle = this.puzzlePool[i];

            if(puzzle.correct) result++;
        }

        return result.toString().concat('/').concat(this.listPuzzle.length.toString());
    }
}
