// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GameManager } from "./GameManager";
import { AnswerData } from "./ViewGamePlay";

const {ccclass, property} = cc._decorator;

@ccclass
export class Answer extends cc.Component {

    @property(cc.Label)
    title: cc.Label = null;

    @property(cc.Button)
    button: cc.Button = null;

    correct: boolean = false;

    isToggle: boolean = false;

    inited: boolean = false;

    loadData(data: AnswerData, resetListAnswer: cc.Component.EventHandler, checkCorrect: cc.Component.EventHandler)
    {
        this.title.string = data.answerString;

        this.correct = data.correct;

        this.onReset();

        if(this.inited) return;
        this.inited = true;

        this.button.clickEvents.splice(0, this.button.clickEvents.length);

        this.button.clickEvents.push(resetListAnswer);

        GameManager.instance.addClickEventHandler(this.button, this.node, 'Answer', 'onClickButton');

        this.button.clickEvents.push(checkCorrect);
    }

    onClickButton()
    {
        this.isToggle = true;

        this.onRefresh();
    }

    onRefresh()
    {
        this.title.node.color = this.isToggle ? cc.Color.WHITE : cc.Color.BLACK;
        this.node.color = this.isToggle ? cc.Color.BLUE : cc.Color.WHITE;
    }

    onReset()
    {
        this.isToggle = false;

        this.onRefresh();
    }
}
