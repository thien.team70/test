// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { SubjectButton } from "./SubjectButton";
import { SubjectData } from "./ViewGamePlay";

const {ccclass, property} = cc._decorator;

@ccclass
export class ViewSubject extends cc.Component 
{
    @property(cc.Prefab)
    subjectPrefab: cc.Prefab = null;

    @property(cc.Node)
    prefabContent: cc.Node = null;

    subjectPool: Array<SubjectButton> = new Array();

    loadData(listData: Array<SubjectData>)
    {
        var poolLength = this.subjectPool.length;
        var dataLength = listData.length;

        for (var i = 0; i < dataLength; i++) 
        {
            var data = listData[i];

            if(i < poolLength)
            {
                this.subjectPool[i].loadData(data);
                continue;
            }

            var prefab = cc.instantiate(this.subjectPrefab);
            prefab.parent = this.prefabContent;

            var subject = prefab.getComponent(SubjectButton);
            if(subject != null) this.subjectPool.push(subject);

            subject.loadData(data);
        }

        for(var i = dataLength; i < poolLength; i++)
        {
            this.subjectPool[i].node.active = false;
        }

        this.viewShow();
    }

    viewShow()
    {
        this.node.active = true;
    }

    viewHide()
    {
        this.node.active = false;
    }
}
