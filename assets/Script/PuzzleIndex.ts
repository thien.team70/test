// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GameManager } from "./GameManager";

const {ccclass, property} = cc._decorator;

@ccclass
export class PuzzleIndex extends cc.Component 
{
    @property(cc.Button)
    button: cc.Button = null;

    @property(cc.Label)
    indexTitle: cc.Label = null;

    @property(cc.Sprite)
    bg: cc.Sprite = null;

    @property(cc.SpriteFrame)
    bgSprite: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    checkSprite: cc.SpriteFrame = null;

    correct: boolean = false;

    answerChoiceIndex: number = -1;

    isToggle: boolean = false;

    inited: boolean = false;

    _index: number = 0;

    loadData(index: number, refreshListPuzzle: cc.Component.EventHandler, showPuzzle: cc.Component.EventHandler)
    {
        this.node.active = true;

        this.correct = false;

        this.answerChoiceIndex = -1;

        this._index = index;

        this.indexTitle.string = (index + 1).toString();

        this.onReset();

        this.setBG(false);

        if(this.inited) return;
        this.inited = true;

        this.button.clickEvents.splice(0, this.button.clickEvents.length);

        this.button.clickEvents.push(refreshListPuzzle);

        GameManager.instance.addClickEventHandler(this.button, this.node, 'PuzzleIndex', 'onClickButton');

        this.button.clickEvents.push(showPuzzle);
    }

    onReset()
    {
        this.isToggle = false;

        this.bg.node.color = cc.Color.WHITE;
    }

    onClickButton()
    {
        this.isToggle = true;

        this.bg.node.color = cc.Color.BLUE;
    }

    setBG(isAnswer: boolean)
    {
        this.bg.spriteFrame = isAnswer ? this.checkSprite : this.bgSprite;

        this.indexTitle.node.active = isAnswer == false;
    }
}
